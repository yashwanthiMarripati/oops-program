package com.yashu;
public class ConstructorExample {

	   int age;
	   String name;
		
	   ConstructorExample()
	{
		this.name="Yashwanthi";
		this.age=22;
	   }
		
	   ConstructorExample(String n,int a)
	{
		this.name=n;
		this.age=a;
	   }
	   public static void main(String args[]){
		ConstructorExample obj1 = new ConstructorExample();
		ConstructorExample obj2 = new ConstructorExample("Pranthi", 12);
		System.out.println(obj1.name+" "+obj1.age);
		System.out.println(obj2.name+" "+obj2.age);
	   }
	}

